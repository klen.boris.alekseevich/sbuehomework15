#pragma once

void Cycle(bool even, int count) 
{
	for (int i = 0; i < count; i++)
	{
		if (i % 2 == 0 && even == true)
		{
			std::cout << i << "\n";
		}
		else if(i % 2 != 0 && even == false)
		{
			std::cout << i << "\n";
		}
		
	}

}